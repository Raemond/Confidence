//
//  RBWRegisterViewController.h
//  confidence
//
//  Created by Raemond on 4/7/14.
//  Copyright (c) 2014 Raemond. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBWRegisterViewController : UIViewController
- (IBAction)signupButton:(id)sender;
@property UIGestureRecognizer *tapper;

@end
