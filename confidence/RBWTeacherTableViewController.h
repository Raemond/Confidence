//
//  RBWTeacherTableViewController.h
//  confidence
//
//  Created by Raemond on 4/7/14.
//  Copyright (c) 2014 Raemond. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBWTeacherTableViewController : UITableViewController

- (IBAction) unwindToList:(UIStoryboardSegue *) segue;

@end
