//
//  RBWCourseTableViewCell.h
//  confidence
//
//  Created by Raemond on 4/30/14.
//  Copyright (c) 2014 Raemond. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBWCourseTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *schoolLable;
@property (weak, nonatomic) IBOutlet UILabel *courseLabel;

@end
