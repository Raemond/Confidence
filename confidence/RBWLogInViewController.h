//
//  RBWLogInViewController.h
//  confidence
//
//  Created by Raemond on 4/7/14.
//  Copyright (c) 2014 Raemond. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBWLogInViewController : UIViewController
- (IBAction)logInButton:(id)sender;
@property UIGestureRecognizer *tapper;

@end
