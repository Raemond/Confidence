//
//  RBWStudentTableViewController.h
//  confidence
//
//  Created by Raemond on 4/8/14.
//  Copyright (c) 2014 Raemond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBWCourse.h"

@interface RBWStudentTableViewController : UITableViewController

@property RBWCourse *chosen;

@end
