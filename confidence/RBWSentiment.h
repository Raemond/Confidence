//
//  RBWSentiment.h
//  confidence
//
//  Created by Raemond on 4/12/14.
//  Copyright (c) 2014 Raemond. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBWSentiment : NSObject

@property NSString *username;
@property NSString *objectID;
@property NSNumber *value;

@end
