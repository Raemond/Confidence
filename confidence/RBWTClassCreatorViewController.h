//
//  RBWTClassCreatorViewController.h
//  confidence
//
//  Created by Raemond on 4/7/14.
//  Copyright (c) 2014 Raemond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBWCourse.h"

@interface RBWTClassCreatorViewController : UIViewController

@property RBWCourse *course;
@property UIGestureRecognizer *tapper;

@end
